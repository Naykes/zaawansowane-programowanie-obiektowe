﻿using System;
using NUnit.Framework;
using Zadanie1;

namespace UnitTestProject
{
    [TestFixture]
    public class UnitTestZadanie1
    {
        [Test]
        public void TestDivine()
        {
            Mat Test = new Mat();
            Assert.AreEqual(10, Test.DivideNumbers(100, 10));
        }

        [Test]
        public void TestDivineBy0()
        {
            Mat Test = new Mat();
            Assert.Throws<ArgumentException>(() => Test.DivideNumbers(10, 0));
        }
    }
}
