﻿using System;
using NUnit.Framework;
using Zadanie2;

namespace UnitTestProject
{
    [TestFixture]
    class UnitTestZadanie2
    {
        [Test]
        public void TestWplac()
        {
            KontoBankowe Konto = new KontoBankowe();
            Konto.Wplac(1000);
            Assert.AreEqual(Konto.SprawdzSaldo(), 1000);
        }

        [Test]
        public void TestWyplac()
        {
            KontoBankowe Konto = new KontoBankowe();            
            Assert.AreEqual(Konto.Wyplac(1000), false);
        }

        [Test]
        public void TestTransfer()
        {
            KontoBankowe Konto1 = new KontoBankowe(2000);
            KontoBankowe Konto2 = new KontoBankowe(1500);
            Konto1.Transfer(400, Konto2);
            Assert.AreEqual(Konto1.SprawdzSaldo(), 1600);
        }

        [Test]
        public void TestSprawdzSaldo()
        {
            KontoBankowe Konto = new KontoBankowe(1500);
            Assert.AreEqual(Konto.SprawdzSaldo(), 1500);
        }
    }
}
