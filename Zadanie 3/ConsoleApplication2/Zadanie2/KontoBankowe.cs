﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie2
{
    public class KontoBankowe
    {
        private int saldo;

        public KontoBankowe(int pieniadze = 0)
        {
            this.saldo = pieniadze;
        }

        public bool Wplac(int pieniadze)
        {
            this.saldo += pieniadze;
            return true;
        }

        public bool Wyplac(int pieniadze)
        {
            if (this.saldo < pieniadze)
            {
                return false;
            }

            this.saldo -= pieniadze;
            return true;
        }

        public bool Transfer(int pieniadze, KontoBankowe Kb)
        {
            if(this.saldo < pieniadze)
            {
                return false;
            }

            this.saldo -= pieniadze;
            Kb.saldo += pieniadze;
            return true;
        }

        public int SprawdzSaldo()
        {
            return this.saldo;
        }
    }
}
