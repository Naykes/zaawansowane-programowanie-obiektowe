﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie1
{
    public class Mat
    {
        public Double DivideNumbers(double a, double b)
        {
            if (b == 0)
                throw new ArgumentException();
            return a / b;
        }
    }
}
