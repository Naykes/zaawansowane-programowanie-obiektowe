﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dziennik.Core.Models
{
    public class Ocena
    {
        public DateTime data { get; private set; }
        public Student student { get; private set; }
        public decimal wartosc { get; private set; }

        public Ocena(Student student, decimal wartosc, DateTime data)
        {
            if (wartosc > 6 || wartosc < 1)
            {
                throw new ArgumentException();
            }

            this.student = student;
            this.wartosc = wartosc;
            this.data = data;
        }
    }
}
