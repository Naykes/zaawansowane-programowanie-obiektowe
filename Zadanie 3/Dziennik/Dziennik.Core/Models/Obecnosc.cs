﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dziennik.Core.Models
{
    public class Obecnosc
    {
        public Student student { get; private set; }

        public bool wartosc { get; set; }

        public Obecnosc(Student student, bool obecnosc)
        {
            this.student = student;
            this.wartosc = obecnosc;
        }

        public void ZmienObecnosc()
        {
            this.wartosc = !this.wartosc;
        }
    }
}
