﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dziennik.Core.Models
{
    public class Student
    {
        public int nrIndeksu { get; private set; }
        public string imie { get; private set; }
        public string nazwisko { get; private set; }

        public Student(int nrIndeksu, string imie, string nazwisko)
        {
            this.nrIndeksu = nrIndeksu;
            this.imie = imie;
            this.nazwisko = nazwisko;
        }
    }
}
