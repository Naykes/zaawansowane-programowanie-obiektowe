﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dziennik.Core.Models
{
    public class Przedmiot
    {
        public string nazwaPrzedmiotu { get; private set; }
        public Dictionary<DateTime, List<Obecnosc>> zajecia;
        public List<Ocena> Oceny;

        public Przedmiot(string nazwaPrzedmiotu)
        {
            this.nazwaPrzedmiotu = nazwaPrzedmiotu;
            this.Oceny = new List<Ocena>();
            this.zajecia = new Dictionary<DateTime, List<Obecnosc>>();
        }

        public void ZmienNazwePrzedmiotu(string nowaNazwa)
        {
            this.nazwaPrzedmiotu = nowaNazwa;
        }

        public override bool Equals(object obj)
        {
            return this.nazwaPrzedmiotu == ((Przedmiot)obj).nazwaPrzedmiotu ? true : false;
        }

        public override int GetHashCode()
        {
            return this.nazwaPrzedmiotu.GetHashCode();
        }

        public List<Obecnosc> this[DateTime data]
        {
            get
            {
                return this.zajecia[data];
            }
        }

        public List<Ocena> this[int index]
        {
            get
            {
                return this.Oceny.Where(o => o.student.nrIndeksu == index).ToList();
            }
        }
    }
}
