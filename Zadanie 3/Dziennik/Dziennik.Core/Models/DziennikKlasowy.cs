﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dziennik.Core.Controler;

namespace Dziennik.Core.Models
{
    public class DziennikKlasowy
    {
        public string nazwaKlasy;

        public List<Przedmiot> przedmioty;
        public List<Student> studenci;

        public DziennikKlasowy(string nazwaKlasy)
        {
            this.nazwaKlasy = nazwaKlasy;
            this.studenci = new List<Student>();
            this.przedmioty = new List<Przedmiot>();
        }

        public void PrzygotujSprawdzanieObecnosci(string przedmiot)
        {
            Przedmiot przed = przedmioty.Where(p => p.nazwaPrzedmiotu == przedmiot).FirstOrDefault();
            if(przed == null)
            {
                throw new ArgumentException();
            }
            DateTime data = DateTime.Parse(DateTime.Now.ToString());
            List<Obecnosc> obecnosci = new List<Obecnosc>();
            studenci.ForEach(s => obecnosci.Add(new Obecnosc(s,true)));
            przed.zajecia.Add(data, obecnosci);
        }

        public void ZmienObecnosc(string przedmiot, DateTime data, int indeks)
        {
            Przedmiot przed = this[przedmiot];
            Obecnosc ob = przed.zajecia[data].Single(o => o.student.nrIndeksu == indeks);
            ob.ZmienObecnosc();
        }

        public void DodajPrzedmiot(string nazwaPrzedmiotu)
        {
            if(this.przedmioty.Where(p => p.nazwaPrzedmiotu == nazwaPrzedmiotu).Count() > 0)
            {
                throw new ArgumentException();
            }
            this.przedmioty.Add(new Przedmiot(nazwaPrzedmiotu));
        }

        public void OcenStudenta(int indexStudenta, string nazwaPrzedmiotu, decimal wartosc)
        {
            Przedmiot przed = przedmioty.Where(p => p.nazwaPrzedmiotu == nazwaPrzedmiotu).FirstOrDefault();
            if (przed == null)
            {
                throw new ArgumentException();
            }

            przed.Oceny.Add(new Ocena(this[indexStudenta], wartosc, DateTime.Today));
        }

        public decimal LiczSredmia(int indexStudenta, string nazwaPrzedmiotu)
        {
            return this[nazwaPrzedmiotu][indexStudenta].Select(o => o.wartosc).Average();
        }

        public void ZmienNazwePrzedmiotu(string nazwaPrzedmiotu, string nowaNazwa)
        {
            Przedmiot przed = this[nazwaPrzedmiotu];
            przed.ZmienNazwePrzedmiotu(nowaNazwa);
        }

        public Przedmiot this[string index]
        {
            get
            {
                Przedmiot prze = this.przedmioty.Where(p => p.nazwaPrzedmiotu == index).First();
                if (prze == null)
                {
                    throw new ArgumentException();
                }
                return prze;
            }
        }

        public Student this[int index]
        {
            get
            {
                return this.studenci.Single(s => s.nrIndeksu == index);
            }
        }
    }
}
