﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using Dziennik.Core.Models;

namespace Dziennik.Core.Controler
{
    public static class DziennikControler
    {
        public static void Init()
        {
            SQLiteConnection.CreateFile("MyDatabase.sqlite");
            SQLiteConnection m_dbConnection = new SQLiteConnection("Data Source=MyDatabase.sqlite;");
            m_dbConnection.Open();
            SQLiteCommand query = new SQLiteCommand("CREATE TABLE Dziennik( klasa varchar(50));", m_dbConnection);
            query.ExecuteNonQuery();
            query.CommandText = "CREATE TABLE Przedmiot( nazwa varchar(50), klasa varchar(50));";
            query.ExecuteNonQuery();
            query.CommandText = "CREATE TABLE Student( indeks int, imie varchar(50), nazwisko varchar(50), klasa varchar(50));";
            query.ExecuteNonQuery();
            query.CommandText = "CREATE TABLE Obecnosc( przedmiot varchar(50), indeks int, war bit, data datetime );";
            query.ExecuteNonQuery();
            query.CommandText = "CREATE TABLE Ocena( przedmiot varchar(50), indeks int, war decimal(2,1), data datetime);";
            query.ExecuteNonQuery();
            query.CommandText = "INSERT INTO Dziennik (klasa) values ('I');";
            query.ExecuteNonQuery();
            query.CommandText = "INSERT INTO Student (indeks, imie, nazwisko, klasa) values (126570, 'Mateusz', 'Pluta', 'I');";
            query.ExecuteNonQuery();
            query.CommandText = "INSERT INTO Przedmiot (nazwa, klasa) values ('Matematyka', 'I');";
            query.ExecuteNonQuery();
            m_dbConnection.Clone();
        }

        public static DziennikKlasowy Load()
        {
            DziennikKlasowy dziennik;

            if (!System.IO.File.Exists("MyDatabase.sqlite"))
            {
                DziennikControler.Init();
            }
            SQLiteConnection m_dbConnection = new SQLiteConnection("Data Source=MyDatabase.sqlite;Version=3;");

            m_dbConnection.Open();

            SQLiteCommand query = new SQLiteCommand("Select klasa from Dziennik LIMIT 1;", m_dbConnection);
            SQLiteDataReader reader = query.ExecuteReader();

            string klasa = string.Empty;
            while(reader.Read())
            {
                klasa = (string)reader["klasa"];
            }
            reader.Close();
            dziennik = new DziennikKlasowy(klasa);

            query.CommandText = string.Format("Select nazwa from Przedmiot where klasa = '{0}';", klasa);
            reader = query.ExecuteReader();
            while (reader.Read())
            {
                dziennik.DodajPrzedmiot((string)reader[0]);
            }
            reader.Close();

            query.CommandText = string.Format("Select indeks, imie, nazwisko from Student where klasa = '{0}';", klasa);
            reader = query.ExecuteReader();
            while (reader.Read())
            {
                dziennik.studenci.Add(new Student((int)reader[0], (string)reader[1], (string)reader[2]));
            }
            reader.Close();

            foreach (Przedmiot przedmiot in dziennik.przedmioty)
            {
                query.CommandText = string.Format("Select indeks, war, data from Ocena where przedmiot = '{0}';", przedmiot.nazwaPrzedmiotu);
                reader = query.ExecuteReader();
                while (reader.Read())
                {
                    przedmiot.Oceny.Add(new Ocena(dziennik[(int)reader[0]], (decimal)reader[1], (DateTime)reader[2]));
                }
                reader.Close();

                query.CommandText = string.Format("Select indeks, war, data from Obecnosc where przedmiot = '{0}';", przedmiot.nazwaPrzedmiotu);
                reader = query.ExecuteReader();
                while (reader.Read())
                {
                    if(!przedmiot.zajecia.ContainsKey((DateTime)reader[2]))
                    {
                        przedmiot.zajecia.Add((DateTime)reader[2], new List<Obecnosc>());
                    }

                    przedmiot.zajecia[(DateTime)reader[2]].Add(new Obecnosc(dziennik[(int)reader[0]], (bool)reader[1]));
                }
                reader.Close();
            }
            m_dbConnection.Close();

            return dziennik;
        }

        public static void DodajOcene(Ocena ocena, Przedmiot przedmiot)
        {
            SQLiteConnection m_dbConnection = new SQLiteConnection("Data Source=MyDatabase.sqlite;Version=3;");
            SQLiteCommand query = new SQLiteCommand(string.Format("Insert into Ocena (przedmiot, indeks, war, data) values ('{0}', {1}, {2}, '{3}');", przedmiot.nazwaPrzedmiotu, ocena.student.nrIndeksu, ocena.wartosc, ocena.data), m_dbConnection);
            m_dbConnection.Open();
            query.ExecuteNonQuery();
            m_dbConnection.Close();
        }

        public static void DodajPrzedmiot(string dziennik, string nazwaPrzedmiotu)
        {
            SQLiteConnection m_dbConnection = new SQLiteConnection("Data Source=MyDatabase.sqlite;Version=3;");
            SQLiteCommand query = new SQLiteCommand(string.Format("Insert into Przedmiot (nazwa, klasa) values ('{0}','{1}');", nazwaPrzedmiotu, dziennik), m_dbConnection);
            m_dbConnection.Open();
            query.ExecuteNonQuery();
            m_dbConnection.Close();
        }

        public static void EdytujPrzedmiot(string dziennik, string nazwaPrzedmiotu, string nowaNazwa)
        {
            SQLiteConnection m_dbConnection = new SQLiteConnection("Data Source=MyDatabase.sqlite;Version=3;");
            SQLiteCommand query = new SQLiteCommand(string.Format("UPDATE Przedmiot SET nazwa = '{0}' Where nazwa = '{1}' and klasa = '{2}';", nowaNazwa, nazwaPrzedmiotu, dziennik), m_dbConnection);
            m_dbConnection.Open();
            query.ExecuteNonQuery();
            m_dbConnection.Close();
        }

        public static void ZapiszObecnosc(string nazwaPrzedmiotu, DateTime data, List<Obecnosc> obecnosci)
        {
            SQLiteConnection m_dbConnection = new SQLiteConnection("Data Source=MyDatabase.sqlite;Version=3;");
            m_dbConnection.Open();
            foreach (Obecnosc obecnosc in obecnosci)
            {
                SQLiteCommand query = new SQLiteCommand(string.Format("Insert into Obecnosc (przedmiot, indeks, war, data) values ('{0}', {1}, {2}, '{3}');", nazwaPrzedmiotu, obecnosc.student.nrIndeksu, Convert.ToInt32(obecnosc.wartosc), data), m_dbConnection);
                query.ExecuteNonQuery();
            }
            m_dbConnection.Close();
        }

        public static void ZmienObecnosc(string nazwaPrzedmiotu, DateTime data, Obecnosc obecnosc)
        {
            SQLiteConnection m_dbConnection = new SQLiteConnection("Data Source=MyDatabase.sqlite;Version=3;");
            SQLiteCommand query = new SQLiteCommand(string.Format("UPDATE Obecnosc SET war = {2} Where przedmiot = '{0}' and indeks = {1} and data = '{3}';", nazwaPrzedmiotu, obecnosc.student.nrIndeksu, Convert.ToInt32(obecnosc.wartosc), data), m_dbConnection);
            m_dbConnection.Open();
            query.ExecuteNonQuery();
            m_dbConnection.Close();

        }
    }
}
