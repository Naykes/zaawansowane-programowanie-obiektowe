﻿using System;
using Dziennik.Core.Models;
using NUnit.Framework;

namespace UnitTestDziennik
{
    [TestFixture]
    public class UnitTest
    {
        [Test]
        public void TestObecnosc()
        {
            Student student = new Student(123456, "Mateusz", "Pluta");
            Obecnosc obecnosc = new Obecnosc(student, true);
            obecnosc.ZmienObecnosc();
            Assert.AreEqual(false, obecnosc.wartosc);
        }

        [Test]
        public void TestDodaniePrzedmiotu()
        {
            DziennikKlasowy dziennik = new DziennikKlasowy("Test");
            dziennik.DodajPrzedmiot("Programowanie");
            Assert.Contains(new Przedmiot("Programowanie"), dziennik.przedmioty);
        }

        [Test]
        public void TestEdycjaPrzedmiotu()
        {
            Przedmiot przedmiot = new Przedmiot("Programowanie");
            przedmiot.ZmienNazwePrzedmiotu("Bazy Danych");
            Assert.AreEqual("Bazy Danych", przedmiot.nazwaPrzedmiotu);
        }

        [Test]
        public void TestOcenUcznia()
        {
            Student student = new Student(123456, "Mateusz", "Pluta");
            DziennikKlasowy dziennik = new DziennikKlasowy("Test");
            dziennik.studenci.Add(student);
            dziennik.DodajPrzedmiot("Programowanie");
            dziennik.OcenStudenta(123456, "Programowanie", 3);
            Assert.AreEqual(3, dziennik["Programowanie"].Oceny[0].wartosc);
        }

        [Test]
        public void TestOcenUcznia2()
        {
            Student student = new Student(123456, "Mateusz", "Pluta");
            DziennikKlasowy dziennik = new DziennikKlasowy("Test");
            dziennik.studenci.Add(student);
            dziennik.DodajPrzedmiot("Programowanie");
            Assert.Throws<ArgumentException>(() => dziennik.OcenStudenta(123456, "Programowanie", 20));
        }

        [Test]
        public void TestLiczSrednia()
        {
            Student student = new Student(123456, "Mateusz", "Pluta");
            DziennikKlasowy dziennik = new DziennikKlasowy("Test");
            dziennik.studenci.Add(student);
            dziennik.DodajPrzedmiot("Programowanie");
            dziennik.DodajPrzedmiot("Bazy Danych");
            dziennik.OcenStudenta(123456, "Programowanie", 3);
            dziennik.OcenStudenta(123456, "Programowanie", new decimal(4.5));
            dziennik.OcenStudenta(123456, "Bazy Danych", new decimal(3.5));
            dziennik.OcenStudenta(123456, "Programowanie", new decimal(3.5));
            dziennik.OcenStudenta(123456, "Programowanie", 5);
            Assert.AreEqual(4, dziennik.LiczSredmia(123456, "Programowanie"));
        }
    }
}
