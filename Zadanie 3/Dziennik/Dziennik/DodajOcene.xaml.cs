﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Dziennik
{
    /// <summary>
    /// Interaction logic for DodajOcene.xaml
    /// </summary>
    public partial class DodajOcene : Window
    {
        public decimal result;

        public DodajOcene()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if(decimal.TryParse(TextBox.Text, out this.result))
            {
                if(result > 6 || result < 1)
                {
                    Error.Content = "Zły zakres!";
                    return;
                }
                DialogResult = true;
            }
            else
            {
                Error.Content = "Zły format!";
            }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            Error.Content = string.Empty;
        }
    }
}
