﻿using Dziennik.Core.Models;
using Dziennik.Core.Controler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using System.Collections;


namespace Dziennik
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static DziennikKlasowy dziennik;
        private static string przedmiot;

        public MainWindow()
        {
            InitializeComponent();
            dziennik = DziennikControler.Load();
            foreach (Przedmiot przedmiot in dziennik.przedmioty)
            {
                TabControl.Items.Add(this.PrzygotujTab(przedmiot));
            }
            
        }

        private void Dodaj_Click(object sender, RoutedEventArgs e)
        {
            DodajPrzedmiot form = new DodajPrzedmiot();
            if(form.ShowDialog() == true)
            {
                dziennik.DodajPrzedmiot(form.result);
                DziennikControler.DodajPrzedmiot(dziennik.nazwaKlasy, form.result);
                TabControl.Items.Add(this.PrzygotujTab(dziennik.przedmioty.Last()));

            }
        }

        private void Edytuj_Click(object sender, RoutedEventArgs e)
        {
            EdytujPrzedmiot form = new EdytujPrzedmiot();
            if(form.ShowDialog() == true)
            {
                string stara = dziennik.przedmioty[TabControl.SelectedIndex].nazwaPrzedmiotu;
                dziennik.ZmienNazwePrzedmiotu(dziennik.przedmioty[TabControl.SelectedIndex].nazwaPrzedmiotu, form.result);
                DziennikControler.EdytujPrzedmiot(dziennik.nazwaKlasy, stara, form.result);
                ((TabItem)TabControl.SelectedItem).Header = form.result;
            }
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {

        }

        private void MenuItem_Click_3(object sender, RoutedEventArgs e)
        {

        }

        private static DataGrid dG()
        {
            DataGrid grid = new DataGrid();
            grid.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            grid.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            Thickness margin = grid.Margin;
            margin.Top = 10;
            margin.Left = 10;
            margin.Right = 10;
            margin.Bottom = 10;
            grid.Margin = margin;
            grid.Height = 215;
            grid.Width = 471;
            grid.AutoGenerateColumns = true;
            grid.CanUserAddRows = false;   
            return grid;
        }

        private static void grid_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            DateTime data;
            if ( DateTime.TryParse(e.Column.SortMemberPath, out data))
            {
                DataRowView view = e.Row.Item as DataRowView;
                Obecnosc obecnosc = dziennik[przedmiot][data].Single(o => o.student.nrIndeksu == (int)view["Indeks"]);
                CheckBox ck = e.EditingElement as CheckBox;
                obecnosc.wartosc = ck.IsChecked.Value;
                DziennikControler.ZmienObecnosc(przedmiot, data, obecnosc);
            }

        }

        private static DataTable Obecnosci(Przedmiot przedmiot, List<Student> uczniowie)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Indeks", typeof(int));
            dt.Columns.Add("Imie", typeof(string));
            dt.Columns.Add("Nazwisko", typeof(string));
            foreach (var item in przedmiot.zajecia)
            {
                dt.Columns.Add(item.Key.ToString(), typeof(bool));
            }

            dt.Columns[0].ReadOnly = true;
            dt.Columns[1].ReadOnly = true;
            dt.Columns[2].ReadOnly = true;

            foreach (Student student in uczniowie)
            {
                DataRow row = dt.NewRow();
                row["Indeks"] = student.nrIndeksu;
                row["Imie"] = student.imie;
                row["Nazwisko"] = student.nazwisko;
                foreach (var item in przedmiot.zajecia)
                {
                    row[item.Key.ToString()] = item.Value.Single(o => o.student.nrIndeksu == student.nrIndeksu).wartosc;
                }
                dt.Rows.Add(row);
            }

            return dt;
        }

        private static DataTable Oceny(Przedmiot przedmiot, List<Student> uczniowie)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Indeks", typeof(int));
            dt.Columns.Add("Imie", typeof(string));
            dt.Columns.Add("Nazwisko", typeof(string));
            dt.Columns.Add("Oceny", typeof(string));
            dt.Columns.Add("Srednia", typeof(decimal));

            dt.Columns[0].ReadOnly = true;
            dt.Columns[1].ReadOnly = true;
            dt.Columns[2].ReadOnly = true;
            dt.Columns[3].ReadOnly = true;
            dt.Columns[4].ReadOnly = true;

            foreach (Student student in uczniowie)
            {
                DataRow row = dt.NewRow();
                row["Indeks"] = student.nrIndeksu;
                row["Imie"] = student.imie;
                row["Nazwisko"] = student.nazwisko;
                row["Oceny"] = string.Join(";", przedmiot[student.nrIndeksu].Select(o => o.wartosc));
                decimal srednia = przedmiot[student.nrIndeksu].Count() > 0 ? przedmiot[student.nrIndeksu].Select(o => o.wartosc).Average() : 0;
                row["Srednia"] = srednia;
                dt.Rows.Add(row);
            }

            return dt;
        }

        private TabItem PrzygotujTab(Przedmiot przedmiot)
        {
            TabItem tabItem = new TabItem() { Name = przedmiot.nazwaPrzedmiotu, Header = przedmiot.nazwaPrzedmiotu };

            TabControl control = new System.Windows.Controls.TabControl();
            TabItem oceny = new TabItem() { Name = "oceny", Header = "Oceny" };
            TabItem obecnosci = new TabItem() { Name = "obecnosci", Header = "Obecnosci" };

            control.Items.Add(oceny);
            control.Items.Add(obecnosci);

            tabItem.Content = control;

            DataGrid dataOceny = MainWindow.dG();
            DataGrid dataObecnosci = MainWindow.dG();

            dataObecnosci.CellEditEnding += grid_CellEditEnding;

            dataObecnosci.ItemsSource = MainWindow.Obecnosci(przedmiot, dziennik.studenci).DefaultView;
            dataOceny.ItemsSource = MainWindow.Oceny(przedmiot, dziennik.studenci).DefaultView;

            oceny.Content = dataOceny;
            obecnosci.Content = dataObecnosci;
            return tabItem;
        }

        private void Sprawdz_Click(object sender, RoutedEventArgs e)
        {
            dziennik.PrzygotujSprawdzanieObecnosci(dziennik.przedmioty[TabControl.SelectedIndex].nazwaPrzedmiotu);
            ((DataGrid)(((TabItem)((TabControl)((TabItem)TabControl.SelectedItem).Content).Items[1]).Content)).ItemsSource = MainWindow.Obecnosci( dziennik.przedmioty[TabControl.SelectedIndex], dziennik.studenci).DefaultView;
            DziennikControler.ZapiszObecnosc(dziennik.przedmioty[TabControl.SelectedIndex].nazwaPrzedmiotu, dziennik.przedmioty[TabControl.SelectedIndex].zajecia.Last().Key, dziennik.przedmioty[TabControl.SelectedIndex].zajecia.Last().Value);
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            przedmiot = dziennik.przedmioty[TabControl.SelectedIndex].nazwaPrzedmiotu;
        }

        private void DodajOcene_Click(object sender, RoutedEventArgs e)
        {
            DataGrid dG = ((DataGrid)(((TabItem)((TabControl)((TabItem)TabControl.SelectedItem).Content).Items[0]).Content));
            if(dG.SelectedItems.Count == 0)
            {
                MessageBox.Show("Nie wybrano elementów");
                return;
            }

            DodajOcene form = new DodajOcene();

            if(form.ShowDialog() == true)
            {
                IList lista = dG.SelectedItems;
                foreach (DataRowView item in lista)
                {
                    dziennik.OcenStudenta((int)item.Row["indeks"], przedmiot, form.result);
                    DziennikControler.DodajOcene(dziennik[przedmiot].Oceny.Last(), dziennik[przedmiot]);
                }
            }
            dG.ItemsSource = MainWindow.Oceny(dziennik.przedmioty[TabControl.SelectedIndex], dziennik.studenci).DefaultView;
        }
    }
}
