﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Dziennik
{
    /// <summary>
    /// Interaction logic for DadajPrzedmiot.xaml
    /// </summary>
    public partial class DodajPrzedmiot : Window
    {
        public string result;

        public DodajPrzedmiot()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.result = this.textBox.Text;
            this.DialogResult = true;
        }
    }
}
