﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie2
{
    abstract class Bohater : IBohater
    {
        protected int Hp;
        protected int Atak;
        protected Rasa rasa;

        public void BijWroga(Bohater enemy)
        {
            enemy.Hp -= this.Atak;
        }

        public int SprawdzHp()
        {
            return this.Hp;
        }

        public override string ToString()
        {
            return string.Format("Bohater: {0}, ma {1} Hp", this.GetType().Name, this.Hp);
        }

    }
}
