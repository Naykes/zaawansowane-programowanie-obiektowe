﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie2
{
    class Rzezimieszek : Bohater
    {
        private int energia;

        public Rzezimieszek(Rasa rasa)
        {
            this.Hp = 150;
            this.Atak = 40;
            this.rasa = rasa;
            this.energia = 200;
        }
    }
}
