﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie2
{
    class Mag : Bohater
    {
        private int mana;

        public Mag(Rasa rasa)
        {
            this.Hp = 100;
            this.mana = 200;
            this.Atak = 20;
            this.rasa = rasa;
        }

        public void RzucCzar(Bohater enemy)
        {
            if (this.mana >= 25)
            {
                this.mana -= 25;
                this.Atak = 250;
                this.BijWroga(enemy);
                this.Atak = 20;
            }
        }

        public override string ToString()
        {
            return base.ToString() + ", mana:" + this.mana;
        }

    }
}
