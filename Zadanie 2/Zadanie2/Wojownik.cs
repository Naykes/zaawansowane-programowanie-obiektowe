﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie2
{
    class Wojownik : Bohater
    {
        public Wojownik(Rasa rasa)
        {
            this.Hp = 200;
            this.Atak = 50;
            this.rasa = rasa;
        }

        public void RzutToporem(Bohater enemy)
        {
            this.Atak = 100;
            this.BijWroga(enemy);
            this.Atak = 50;
        }
    }
}
