﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Zadanie2
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Bohater> DruzynaPierscienia = new List<Bohater>();

            Mag mag = new Mag(Rasa.Elf);
            Wojownik woj = new Wojownik(Rasa.Ludz);
            Rzezimieszek rze = new Rzezimieszek(Rasa.Krasnolud);
            DruzynaPierscienia.Add(mag);
            DruzynaPierscienia.Add(woj);
            DruzynaPierscienia.Add(rze);

            DruzynaPierscienia.ForEach(b => Console.WriteLine(b.ToString()));

            Enemy Uruk_Hai = new Enemy();
            Random dice = new Random();
            int tura = 1;
            while (DruzynaPierscienia.Count > 0)
            {
                DruzynaPierscienia.ForEach(d => d.BijWroga(Uruk_Hai));
                int wybor = dice.Next(0, DruzynaPierscienia.Count);
                Uruk_Hai.BijWroga(DruzynaPierscienia[wybor]);
                int czar = dice.Next(1, 7);
                int axa = dice.Next(1, 7);
                if (czar > 4 && DruzynaPierscienia.Contains(mag))
                {
                    mag.RzucCzar(Uruk_Hai);
                    Console.WriteLine("Mag rzuca czara!!!");
                }
                if (axa == 6 && DruzynaPierscienia.Contains(woj))
                {
                    woj.RzutToporem(Uruk_Hai);
                    Console.WriteLine("Axem go!!!");
                }
                Console.WriteLine("Tura:{0}", tura++);
                DruzynaPierscienia.ForEach(b => Console.WriteLine(b.ToString()));
                Console.WriteLine(Uruk_Hai);
                if (Uruk_Hai.SprawdzHp() < 0)
                    break;
                for (int i = DruzynaPierscienia.Count - 1; i >= 0; i--)
                {
                    if (DruzynaPierscienia[i].SprawdzHp() < 0)
                        DruzynaPierscienia.RemoveAt(i);
                }
            }
            Console.ReadKey();

        }
    }
}
