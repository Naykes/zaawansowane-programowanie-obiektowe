﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie2
{
    interface IBohater
    {
        void BijWroga(Bohater enemy);
        int SprawdzHp();
    }
}
