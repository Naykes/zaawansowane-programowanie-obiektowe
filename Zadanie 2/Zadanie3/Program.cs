﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Zadanie3
{
    class Program
    {
        static void Main(string[] args)
        {
            Student asd = new Student("Mateusz", "Pluta", "1993.09.23", 1, 1, 126570);
            asd.DodajOcene(new Ocena("Programowanie", DateTime.Now.ToString(), 4.5));
            asd.WypiszInfo();
            asd.WypiszOceny();

            Console.ReadKey();
        }
    }
}
