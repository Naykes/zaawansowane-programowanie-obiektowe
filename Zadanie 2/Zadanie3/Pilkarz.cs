﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Zadanie3
{
    class Pilkarz : Osoba
    {
        string pozycja;
        string klub;
        int liczbaGoli = 0;

        public Pilkarz(string imie_, string nazwisko_, string dataUrodzenia_, string pozycja_, string klub_)
            : base(imie_,nazwisko_,dataUrodzenia_)
        {
            this.pozycja = pozycja_;
            this.klub = klub_;
        }

        public override void WypiszInfo()
        {
            base.WypiszInfo();
            Console.WriteLine("Pozycja:{0}, Klub:{1}", this.pozycja, this.klub);
        }

        public virtual void StrzelGola()
        {
            this.liczbaGoli++;
        }
    }
}
