﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Zadanie3
{
    class Ocena
    {
        private string nazwaPrzedmiotu;

        public string NazwaPrzedmiotu
        {
            get { return this.nazwaPrzedmiotu; }
            set { this.nazwaPrzedmiotu = value; }
        }

        private string data;

        public string Data
        {
            get { return this.data; }
            set { this.data = value; }
        }

        private double wartosc;

        public double Wartosc
        {
            get { return this.wartosc; }
            set { this.wartosc = value; }
        }


        public Ocena(string nazwaPrzedmiotu_, string data_, double wartosc_)
        {
            this.nazwaPrzedmiotu = nazwaPrzedmiotu_;
            this.data = data_;
            this.wartosc = wartosc_;
        }

        public void WypiszInfo()
        {
            Console.WriteLine("{0} : {1} : {2}", this.data, this.nazwaPrzedmiotu, this.wartosc);
        }
    }
}
