﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Zadanie3
{
    class Osoba
    {
        protected string imie;
        protected string nazwisko;
        protected string dataUrodzenia;

        public Osoba(string imie_, string nazwisko_, string dataUrodzenia_)
        {
            this.imie = imie_;
            this.nazwisko = nazwisko_;
            this.dataUrodzenia = dataUrodzenia_;
        }

        public virtual void WypiszInfo()
        {
            Console.WriteLine("Imię:{0}, Nazwisko:{1}, Data urodzenia: {2}", this.imie, this.nazwisko, this.dataUrodzenia);
        }
    }
}
