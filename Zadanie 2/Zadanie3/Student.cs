﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Zadanie3
{
    class Student : Osoba
    {
        int rok;
        int grupa;
        int nrIndexu;
        List<Ocena> oceny = new List<Ocena>();

        public Student(string imie_, string nazwisko_, string dataUrodzenia_, int rok_, int grupa_, int nrIndexu_)
            : base(imie_, nazwisko_, dataUrodzenia_)
        {
            this.rok = rok_;
            this.grupa = grupa_;
            this.nrIndexu = nrIndexu_;
        }

        public override void WypiszInfo()
        {
            base.WypiszInfo();
            Console.WriteLine("Student {0} roku, grupa {1}, nr indeksu {2}", this.rok, this.grupa, this.nrIndexu);
        }

        public void DodajOcene(Ocena ocena)
        {
            this.oceny.Add(ocena);
        }

        public void WypiszOceny()
        {
            this.oceny.ForEach(o => o.WypiszInfo());
        }

        public void WypiszOceny(string nazwaPrzedmiotu)
        {
            this.oceny.Where(o => o.NazwaPrzedmiotu == nazwaPrzedmiotu).ToList().ForEach(o => o.WypiszInfo());
        }

        public void usunOcene(string nazwaPrzedmiotu, string data, double wartosc)
        {
            this.oceny.RemoveAll(o => (o.NazwaPrzedmiotu == nazwaPrzedmiotu && o.Data == data && o.Wartosc == wartosc));
        }

        public void usunOceny()
        {
            this.oceny.RemoveAll(o => 1 == 1);
        }

        public void usunOceny(string nazwaPrzedmiotu)
        {
            this.oceny.RemoveAll(o => o.NazwaPrzedmiotu == nazwaPrzedmiotu);
        }

    }
}
