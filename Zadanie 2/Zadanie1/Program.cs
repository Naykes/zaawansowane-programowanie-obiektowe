﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<ISamochod> samochody = new List<ISamochod>();

            samochody.Add(new RollsRoyce());
            samochody.Add(new RangeRover());
            samochody.Add(new AstonMartin());

            samochody.ForEach(s => Console.WriteLine("Marka:{0}, Salon:{1}", s.WypiszMarke(), s.WypiszSalon()));

            Console.ReadKey();
        }

    }
}
