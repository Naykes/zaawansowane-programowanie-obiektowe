﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabryka
{
    class Program
    {
        static void Main(string[] args)
        {
            Producent[] producenci = new Producent[2];

            producenci[0] = new Apple();
            producenci[1] = new Samsung();

            foreach (Producent producent in producenci)
            {
                Console.WriteLine(producent);
                Console.WriteLine(producent.DawajEkran());
                Console.WriteLine(producent.DawajObudowe());
                Console.WriteLine();
            }

            Console.ReadKey();
        }
    }
}
