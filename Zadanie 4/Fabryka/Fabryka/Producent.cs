﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabryka
{
    abstract class Producent
    {
        protected string nazwaProducenta;

        public Producent(string NazwaProducenta)
        {
            this.nazwaProducenta = NazwaProducenta;
        }

        public abstract Obudowa DawajObudowe();
        public abstract Ekran DawajEkran();

        public override string ToString()
        {
            return string.Format("Producent: {0}", this.nazwaProducenta);
        }
    }
}
