﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabryka
{
    abstract class Obudowa
    {
        protected decimal wysokosc;
        protected decimal dlugosc;
        protected decimal grubosc;

        public Obudowa(decimal Wysokosc, decimal Dlugosc, decimal Grubosc)
        {
            this.wysokosc = Wysokosc;
            this.dlugosc = Dlugosc;
            this.grubosc = Grubosc;
        }

        public override string ToString()
        {
            return string.Format("Obudowa: Wysokosc:{0}mm, Długość:{1}mm, Grubość:{2}mm", this.wysokosc, this.dlugosc, this.grubosc);
        }
    }
}
