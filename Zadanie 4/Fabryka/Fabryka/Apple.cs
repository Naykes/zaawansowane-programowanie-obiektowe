﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabryka
{
    class Apple : Producent
    {
        public Apple()
            :base("Apple")
        {
        }

        public override Ekran DawajEkran()
        {
            return new EkranApple();
        }

        public override Obudowa DawajObudowe()
        {
            return new ObudowaApple();
        }
    }
}
