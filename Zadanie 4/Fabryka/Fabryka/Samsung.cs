﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabryka
{
    class Samsung : Producent
    {
        public Samsung()
            :base("Samsung")
        {
        }

        public override Ekran DawajEkran()
        {
            return new EkranSamsung();
        }

        public override Obudowa DawajObudowe()
        {
            return new ObudowaSamsung();
        }
    }
}
