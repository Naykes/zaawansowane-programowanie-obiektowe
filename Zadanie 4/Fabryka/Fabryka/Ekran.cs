﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabryka
{
    abstract class Ekran
    {
        protected decimal przekatna;

        public Ekran(decimal PrzekatnaEkranu)
        {
            this.przekatna = PrzekatnaEkranu;
        }

        public override string ToString()
        {
            return string.Format("Ekran o przekątnej: {0}cala.", this.przekatna);
        }
    }
}
