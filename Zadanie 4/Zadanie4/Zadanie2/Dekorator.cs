﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie2
{
    public abstract class Dekorator : ITelefon
    {
        public ITelefon telefon;

        public Dekorator(ITelefon telefon)
        {
            this.telefon = telefon;
        }

        public string WybierzNumer(string numer)
        {
            return telefon.WybierzNumer(numer);
        }

        public string WyslijWiadomosc(string wiadomosc)
        {
            return this.telefon.WyslijWiadomosc(wiadomosc);
        }

        public virtual string PokazTyp()
        {
            return this.telefon.PokazTyp();
        }
    }
}
