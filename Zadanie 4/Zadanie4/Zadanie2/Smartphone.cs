﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie2
{
    class Smartphone : Dekorator
    {
        string _rodzaj;

        public Smartphone(ZwyklyTelefon telefon, string rodzaj)
            : base(telefon)
        {
            this._rodzaj = rodzaj;
        }

        public override string PokazTyp()
        {
            return _rodzaj;
        }

        public string Bieda()
        {
            return "zawiecha!!";
        }

        public string Pac()
        {
            return "Gwarancja tego nie obejmuje!!";
        }
    }
}
