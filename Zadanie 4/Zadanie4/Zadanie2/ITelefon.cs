﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie2
{
    public interface ITelefon
    {
        string WybierzNumer(string numer);
        string WyslijWiadomosc(string wiadomosc);
        string PokazTyp();
    }
}
