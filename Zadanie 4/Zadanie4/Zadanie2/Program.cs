﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie2
{
    class Program
    {
        static void Main(string[] args)
        {
            ZwyklyTelefon telefon = new ZwyklyTelefon();
            Smartphone sm = new Smartphone(telefon, "LG");

            Console.WriteLine(telefon.PokazTyp());
            Console.WriteLine(telefon.WybierzNumer("123456789"));
            Console.WriteLine(telefon.WyslijWiadomosc("Bieda"));
                         
            Console.WriteLine(sm.PokazTyp());
            Console.WriteLine(sm.WybierzNumer("123456789"));
            Console.WriteLine(sm.WyslijWiadomosc("Bieda"));
            Console.WriteLine(sm.Bieda());
            Console.WriteLine(sm.Pac());

            Console.ReadKey();
        }
    }
}
