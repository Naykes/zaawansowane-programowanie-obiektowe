﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie2
{
    class ZwyklyTelefon : ITelefon
    {
        private string typ = "ZwyklyTelefon";
        public virtual string WybierzNumer(string numer)
        {
            return string.Format("Wybrano numer: {0}", numer);
        }

        public virtual string WyslijWiadomosc(string wiadomosc)
        {
            return "Wiadomosc wysłana";
        }

        public virtual string PokazTyp()
        {
            return this.typ;
        }
    }
}
