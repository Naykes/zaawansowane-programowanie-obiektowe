﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Uczelnia UWM = new Uczelnia(new DateTime(2017, 2, 14));
            //Uczelnia Stanford = new Uczelnia(new DateTime(2017, 3, 15));

            Uczelnia UWM = Uczelnia.Instance;
            Uczelnia Stanford = Uczelnia.Instance;


            UWM.RekrutujStudentow();
            Stanford.RekrutujStudentow();
            Console.ReadKey();
        }
    }
}
