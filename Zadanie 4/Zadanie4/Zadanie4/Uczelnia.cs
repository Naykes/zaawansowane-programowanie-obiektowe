﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie1
{
    class Uczelnia
    {
        private DateTime DataRekrutacji;

        private static Uczelnia instance; 

        private Uczelnia()
        {

        }

        static Uczelnia()
        {

        }

        public void RekrutujStudentow()
        {
            Console.WriteLine("Data rekrutacji:{0:dd MMMM yyyy}", DataRekrutacji);
        }

        public static Uczelnia Instance
        {
            get
            {
                if(instance == null)
                    instance = new Uczelnia() { DataRekrutacji = DateTime.Today };
                return instance;
            }
        }

    }
}
